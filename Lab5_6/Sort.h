#pragma once

//Interface implementuj�cy podstawowe 
//	metody potrzebne do algorytm�w sortowania
// Obs�uguje dowolny  typ liczbowy

template<class T> class Sort {
public:
	// Metoda wirtualna wy�wietlaj�ca zawarto�� tablicy
	virtual void show() = 0; 
	// Konsrtuktor parametryczny definiuj�cy rozmmiar tablicy
	explicit Sort(int l) {   
		data = new T[l];
		size = l;
		helper = new T[l];
	}
 // Konstruktor bezparametryczny pozwala na budowanie tablicy obiekt�w opartych o interace
	explicit Sort() {   
		data = NULL;
		size = 0;
		helper = NULL;
	}
	 // W wypadku obiektu utworzonego bez parametru,
	//ustali� mo�na rozmiar (tak�e zmieni� wielko�� ju� utworzonego obiektu)
	void setSize(int s) {  
		data = new T[s];
		size = s;
		helper = new T[s];
	}
	// Metoda sprawdza czy do danego elementu tablica jest posortowana
	bool isSorted(int to){
		for(int i=0;i<to;i++){
			if(data[i]>data[i+1]){
			return false;
			}else{
			return  true;
			}
		}
		return true;
	}
	// Zape�nia tablice calkowitymi liczbami pseudolosowymi
	void randomPopulate() {
		srand(time(NULL));
		for (int i = 0; i < size; i++) {
			data[i] = rand() / 10;
		}
	}
	// Sprawdza czy cala tablica jest posortowana 
	bool check() {
		for (int i = 1; i < size; i++) {
			T tmp = data[i];
			T tmp2 = data[i - 1];
			if (tmp < tmp2) {
				cerr << "Table not sorted at " << i << endl;
				return false;
			}
		}
		return true;
	}
	// Odwraca kolejnosc elementow w tablicy
	void reverse() {		
			for (int i = 0; i < size; i++) {
				if (i > size - 1 - i) {
					break;
				}
				T tmp = data[i];
				data[i] = data[size - i-1];
				data[size - i - 1] = tmp;
			}

	}
protected:
	// Glowna tablica
	T * data;
	//rozmiar tablicy
	int size;
	// Tablica pomocnicza 
	T * helper;
};
