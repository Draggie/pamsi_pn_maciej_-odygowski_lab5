#pragma once
#include "QuickSort.h"
/*Klasa implementuj�ca sortowanie Shella, dziedziczy interfejs Sort oraz metod� sort z QuickSort - w celu sortowania cz�ci tablicy (do pomiar�w)*/

template <class T> class ShellSort : public QuickSort<T> {
public:
	/*Odziedziczony publiczny konstruktor*/
	using QuickSort<T>::QuickSort;
	/*Odziedziczona metoda wy�wietlania zawarto�ci tablicy*/
	void show();
	/*Metoda sortowania algorytmem Shella*/
	void shellSort();
	/*Metoda pozwalaj�ca na wyznaczenie optymalnej warto�ci kroku*/
	long getGap();
};


template <class T> long ShellSort<T>::getGap() {
	double gap = (pow(3, size) - 1) / 2;
	if (gap > (size / 3)) {
		gap = size / 3;
	}
	return (long)gap;
}

template <class T> void ShellSort<T>::shellSort() {
	int j;
	for (int gap = getGap(); gap > 0; gap /= 2) {
		for (int i = gap; i < size; ++i) {
			int tmp = data[i];
			for (j = i; j >= gap && tmp < data[j - gap]; j -= gap) {
				data[j] = data[j - gap];
			}
			data[j] = tmp;
		}
	}
}

template <class T> void ShellSort<T>::show() {
	cout << "Zawartosc tablicy: " << endl;
	for (int i = 0; i < size; i++) {
		cout << data[i] << endl;
	}
}