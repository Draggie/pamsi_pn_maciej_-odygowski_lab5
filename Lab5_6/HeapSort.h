#pragma once
#include "Sort.h"
#include "QuickSort.h"
template <class T> class HeapSort : public QuickSort<T> {
public:
	void show();
	void heapSort(int);
	void heapify(long,int);
	void buildHeap(int);
	using QuickSort<T>::QuickSort;
};

template <class T > void HeapSort<T>::buildHeap(int N) {
	for (int i = N / 2; i > 0; i--) {
		heapify(N, i);
	}
}

template <class T> void HeapSort<T>::heapSort(int N) {
	
	buildHeap(N);
	for (int i = N; i > 1; i--) {
		swap(i, 1);
		N--;
		heapify(N, 1);
	}

}
//long i;
//for (i = N / 2; i > 0; --i) {
//	heapify(i, N);
//}
//for (i = N - 1; i > 0; --i) {
//	swap(0, i);
//	heapify(1, i);
//}

template <class T> void HeapSort<T>::show() {
	cout << "Zawartosc tablicy" << endl;
	for (int i = 0; i < size; i++) {
		cout << data[i] << endl;
	}
}

//template <class T> void HeapSort<T>::heapify(long i, int N) {
//	long j;
//	while (i <= N / 2) {
//		j = 2 * i;
//		if (j + 1 <= N && data[j + 1] > data[j]) {
//			j = j + 1;
//		}
//		if (data[i] < data[j]) {
//			swap(i, j);
//		}
//		else {
//			break;
//		}
//		i = j;
//	}
//}

template <class T> void HeapSort<T>::heapify(long heapSize, int i) {
	int largest;
	int l = 2 * i, r = (2 * i) + 1;
	if (l <= heapSize && data[l > data[i]]) {
		largest = l;
	}
	else {
		largest = i;
	}
	if (r <= heapSize && data[r] > data[largest]) {
		largest = r;
	}
	if (largest != i) {
		swap(largest, i);
		heapify(heapSize, largest);
	}
}