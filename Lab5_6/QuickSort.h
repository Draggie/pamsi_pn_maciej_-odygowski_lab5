#pragma once
#ifndef QUICKSORT_H
#define QUICKSORT_H
#include <time.h>
#include <stdlib.h>
#include "Sort.h"
/*Klas implementuje algorytm szybkiego sortowania, dziedziczy metody publiczne z interfejsu Sort*/
template <class T> class QuickSort : public Sort<T> {
public:
	/*Odziedziczony publiczny konstruktor*/
	using Sort<T>::Sort;
	/*Destruktor klasy*/
	~QuickSort() {
		delete[] data;
	}
	/*Metoda zamieniająca miejscami w tablicy elementy o podanych indeksach (a,b)*/
	void swap(int  , int  );
	/*Metoda implementująca algorytm sortowania szybkiego*/
	void sort(int  , int  );
	/*Wymagana implementacja wirtualnej metody wyświetlenia danych*/
	void show();
	/*Metoda zwracająca podzielone z pomocą pivota tablice */
	T partition(int l, int );
	/*Metoda mediany z trzech - pozwala na szybkie ustalenie pivota*/
	void median(int & , int &);
};


#endif 

template<class T> void QuickSort<T>::median(int & left, int & right) {
	if (data[++left - 1] > data[--right]) {
		swap(left - 1, right);
	}
	if (data[left - 1] > data[right / 2]) {
		swap(left - 2, right / 2);
	}
	if (data[right / 2] > data[right]) {
		swap(right / 2, right);
		swap(right / 2, right - 1);
	}
}

template<class T> T QuickSort<T>::partition(int first, int last) {
	
	int pivot, mid =(first+last)/2;
	pivot = data[first] > data[mid] ? first : mid;

	if(data[pivot] > data[last]){
	pivot=last;
	}

	swap(pivot,first);
	pivot=first;
	while(first<last){
		if(data[first] <= data[last]){
		swap(pivot++,first);
		}
		++first;
	}

	swap(pivot,last);
	return pivot;
	
	

}


template <class T> void QuickSort<T>::swap(int  a, int b) {
	T tmp = data[b];
	data[b] = data[a];
	data[a] = tmp;
}

template <class T> void QuickSort<T>::sort(int  left, int  right) {
	int i = left;
	int j = right;
	T x = data[(left + right) / 2];
	do {
		while (data[i] < x)
			i++;
		while (data[j] > x)
			j--;
		if (i <= j) {
			swap(i, j);
			i++;
			j--;
		}
	} while (i <= j);
	if (left < j)
		sort(left, j);
	if (right > i)
		sort(i, right);
}



template <class T> void QuickSort<T>::show() {
	cout << "Zawartosc tablicy: " << endl;
	for (int i = 0; i < size; i++) {
		cout << data[i] << endl;
	}
}