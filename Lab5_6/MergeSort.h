#pragma once
// Klasa dziedziczy metody publiczne interfejsu Sort<T>, implementuje metody szczegolne dla 
// algorytmu sortowania przez scalanie
#include "Sort.h"
template <class T> class MergeSort : public Sort<T> {
public:
	// Metoda scala tablice glowna i pomocnicza w zadanym zakresie
	void merge(int, int, int);
	// Metoda realizujaca algorytm  sortowania
	void mergeSort(int, int);
	// Wymagana metoda wyswietlania
	void show();
	// Odziedziczony konstruktor bezparametryczny
	using Sort<T>::Sort;
	//Destruktor dla tablic 
	~MergeSort() {
		delete[] data, helper;
	}
};


template <class T> void MergeSort<T>::show() {
	cout << "Zawartosc tablicy:" << endl;
	for (int i = 0; i < size; i++) {
		cout << data[i] << endl;
	}
}

template <class T> void MergeSort<T>::merge(int start, int middle, int end) {
	int i, j, q;
	for (i = start; i <= end; i++) {
		helper[i] = data[i];
	}
	i = start;
	j = middle + 1;
	q = start;
	while ((i <= middle) && (j <= end)) {
		if (helper[i] < helper[j]) {
			data[q++]=helper[i++];
		}
		else {
			data[q++] = helper[j++];
		}
	}
	while (i <= middle) {
		data[q++] = helper[i++];
	}
}

template <class T> void MergeSort<T>::mergeSort(int start, int end) {
	int middle;
	if (start < end) {
		middle = (start + end) / 2;
		mergeSort(start, middle);
		mergeSort(middle + 1, end);
		merge(start, middle, end);
	}
}

