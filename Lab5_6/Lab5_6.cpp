// Lab5_6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "QuickSort.h"
#include "MergeSort.h"
#include "IntroSort.h"
#include "HeapSort.h"
#include "IntroSort.h"
#include <math.h>
#include <sstream>
#include <fstream>
#include "ShellSort.h"
#include <ctime>
#include <vector>
#include <Windows.h>
#include <iostream>

using namespace std;
/*Funkcja zamienia kropki na przeciniki*/
string comaRemover(double numb) {
	std::ostringstream strs;
	strs << numb;
	std::string str = strs.str();
	int l= str.find_first_of('.');
	if(l>0 && l<str.length())
	str[l] = ',';
	return str;
}

int main()
{
	int tab[5] = { 10000,50000,100000,500000,1000000 };
	ofstream plik("log5.csv",ios::app);
	for (int k = 0; k < 5; k++) {
		int size = tab[k];
		HeapSort<int> * arr = new HeapSort<int>[100];
		for (int i = 0; i < 100; i++) {
			arr[i].setSize(size);
			arr[i].randomPopulate();
		
		
		}
		LARGE_INTEGER freq;
		LARGE_INTEGER t1, t2;
		double elapsed;


		cout << "Data initialized, starting sorting: " << endl;

		QueryPerformanceFrequency(&freq);

		for (int i = 0; i < 100; i++) {
			clock_t begin = clock();
			QueryPerformanceCounter(&t1);

			arr[i].heapSort(0, size - 1);
			clock_t end = clock();
			double elapsed_time = double(end - begin);

			QueryPerformanceCounter(&t2);
			elapsed = (t2.QuadPart - t1.QuadPart)*1000.0 / freq.QuadPart;
			plik << comaRemover(elapsed) << endl;
			cout << "Data sorted in time of " << elapsed_time << "ms" << "Or in detail : " << elapsed << "ms" << endl;
		}
		plik << "SEPARATOR" << endl;
		for (int i = 0; i < 100; i++) {
			if (!arr[i].check()) {

			}
		}
	}
	system("PAUSE");
    return 0;
}

