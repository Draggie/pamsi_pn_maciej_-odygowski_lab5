#pragma once
#include "Sort.h"
#include "QuickSort.h"
#include "HeapSort.h"
#define M_LN2 0.69314718055994530942

template <class T> class IntroSort : public HeapSort<T> {
public:
	using HeapSort<T>::HeapSort;
	void introSort(int, int);
	void insertionSort(int);
	void hybridIntroSort(int);
};


template <class T> void IntroSort<T>::hybridIntroSort(int N) {
	introSort(N, (int)floor(2 * log(N) / M_LN2));
	insertionSort(N);
}


template <class T> void IntroSort<T>::introSort(int N, int M) {
	long i;
	if (M <= 0) {
		heapSort(N);
		return;
	}
	i = partition(0, N);
	if (i > 9) {
		introSort(i, M - 1);
	}
	if (N - 1 - i > 9) {
		introSort(N - 1 - i, M - 1);
	}

}


template <class T> void IntroSort<T>::insertionSort(int N) {
	long i, j;
	T tmp;

	for (i = 1; i < N; ++i) {
		tmp = data[i];
		for (j = i; j > 0 && tmp < data[j - 1]; --j) {
			data[j] = data[j - 1];
		}
		data[j] = tmp;
	
	}
}